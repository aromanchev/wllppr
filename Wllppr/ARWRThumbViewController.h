//
//  ARWRThumbViewController.h
//  Wllppr
//
//  Created by IT-EFFECTs on 21.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <M13ProgressViewBorderedBar.h>

@interface ARWRThumbViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *thumbView;
@property (nonatomic, strong) M13ProgressViewBorderedBar *progressBar;

- (void)resetProgressBar;

@end
