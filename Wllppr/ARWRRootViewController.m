//
//  ViewController.m
//  Wllppr
//
//  Created by IT-EFFECTs on 20.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import "ARWRRootViewController.h"
#import "ARWRThumbViewController.h"
#import "ARWRAboutViewController.h"

#import <UIImageView+AFNetworking.h>
#import <SCEasingFunction.h>
#import <SCPageViewController.h>
#import <SCParallaxPageLayouter.h>
#import <RKDropdownAlert.h>
#import <StoreKit/StoreKit.h>

static NSInteger ARWRNotificationDuration = 4;

@interface ARWRRootViewController () <SCPageViewControllerDelegate, SCPageViewControllerDataSource>

@property (nonatomic) NSUInteger imageCount;
@property (nonatomic) NSUInteger currentPageIndex;
@property (nonatomic, strong) SCPageViewController *thumbsController;
@property (nonatomic, strong) NSMutableArray *viewControllers;

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, strong) UIVisualEffectView *authorView;
@property (nonatomic, strong) UIButton *authorButton;
@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) UIView *lockscreenView;
@property (nonatomic, strong) UILabel *lockscreenTimeLabel;
@property (nonatomic, strong) UILabel *lockscreenDateLabel;

@end

@implementation ARWRRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageCount = 0;
    self.currentPageIndex = 0;
    self.viewControllers = [NSMutableArray array];
    self.view.backgroundColor = [UIColor arwrWhiteColor];
    
    self.navigationController.navigationBarHidden = YES;
    self.backgroundImageView.tintColor = [UIColor arwrLightBlackColor];
    self.backgroundImageView.backgroundColor = [UIColor arwrWhiteColor];
    
    [self configureCollection];
    [self configureAuthorView];
    [self configureLockscreenView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Configure Collection

- (void)configureCollection
{
    SCPageLayouter *layouter = [[SCParallaxPageLayouter alloc] init];
    layouter.navigationType = SCPageLayouterNavigationTypeHorizontal;
    layouter.numberOfPagesToPreloadAfterCurrentPage = 3;
    layouter.numberOfPagesToPreloadBeforeCurrentPage = 3;
    
    self.thumbsController = [[SCPageViewController alloc] init];
    [self.thumbsController setDelegate:self];
    [self.thumbsController setDataSource:self];
    [self.thumbsController setPagingEnabled:YES];
    [self.thumbsController setContinuousNavigationEnabled:NO];
    [self.thumbsController setAnimationDuration:1.5f];
    [self.thumbsController setLayouter:layouter animated:YES completion:nil];
    [self.thumbsController setEasingFunction:[SCEasingFunction easingFunctionWithType:SCEasingFunctionTypeElasticEaseOut]];
    
    [self addChildViewController:self.thumbsController];
    [self.thumbsController.view setFrame:self.view.bounds];
    [self.view addSubview:self.thumbsController.view];
    [self.thumbsController didMoveToParentViewController:self];
}

- (void)checkUpdate
{
//    [[NSUserDefaults standardUserDefaults] setValue:@NO forKey:@"WLLPPR_1_3_UPDATE_NOTIFICATION"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"WLLPPR_1_3_UPDATE_NOTIFICATION"]) {
        [RKDropdownAlert title:NSLocalizedString(@"Hey!", nil)
                       message:NSLocalizedString(@"Swipe left or right to change the image.\nSwipe down to save the image you liked.", nil)
               backgroundColor:[UIColor arwrYellowColor]
                     textColor:[UIColor arwrLightBlackColor] time:60];
        [[NSUserDefaults standardUserDefaults] setValue:@YES forKey:@"WLLPPR_1_3_UPDATE_NOTIFICATION"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - Authot View

- (void)configureAuthorView
{
    int x = (IS_IPHONEX) ? 17 : 0;
    float h = 44.f + x;
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    self.authorView = [[UIVisualEffectView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([[UIScreen mainScreen] bounds]) - h, CGRectGetWidth([[UIScreen mainScreen] bounds]), h)];
    self.authorView.effect = blurEffect;
    self.authorView.backgroundColor = [UIColor clearColor];
    self.authorView.hidden = YES;
    [self.view addSubview:self.authorView];
    
    self.saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.saveButton setFrame:CGRectMake(0.f, 0.f, 0.25 * CGRectGetWidth(self.authorView.bounds), 44.f)];
    [self.saveButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.saveButton.titleLabel setFont:[UIFont fontWithName:@"Menlo-Regular" size:14.f]];
    [self.saveButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateNormal];
    [self.saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.saveButton setEnabled:YES];
    [self.saveButton addTarget:self action:@selector(saveImage) forControlEvents:UIControlEventTouchUpInside];
    
    int x1 = (IS_IPHONEX) ? 34 : 44;
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.saveButton.frame), 0.f, 1.f, x1)];
    [separator setBackgroundColor:[UIColor arwrWhiteColor]];
    
    self.authorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.authorButton setFrame:CGRectMake(CGRectGetMaxX(self.saveButton.frame), 0.f, 0.75 * CGRectGetWidth(self.authorView.bounds), 44.f)];
    [self.authorButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.authorButton.titleLabel setFont:[UIFont fontWithName:@"Menlo-Regular" size:14.f]];
    [self.authorButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateNormal];
    [self.authorButton setTitle:@"" forState:UIControlStateNormal];
    [self.authorButton setEnabled:NO];
    [self.authorButton addTarget:self action:@selector(openAuthorPage) forControlEvents:UIControlEventTouchUpInside];
    
    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    UIVisualEffectView *vibrancyView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    vibrancyView.frame = self.authorView.bounds;
    [vibrancyView.contentView addSubview:self.saveButton];
    [vibrancyView.contentView addSubview:self.authorButton];
    [vibrancyView.contentView addSubview:separator];
    
    [self.authorView.contentView addSubview:vibrancyView];
}

- (void)openAuthorPage
{
    if (self.currentPageIndex == 30) return;
    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASOpenAuthorPageOnUnsplash];
    [self openLink:[[ARWRUnsplashManager sharedManager] linktToPageOfAuthorOfImageAtIndex:self.currentPageIndex]];
}

- (void)updateAuthor:(NSString *)authorName
{
    [self.authorButton setTitle:[NSString stringWithFormat:@"%@", authorName] forState:UIControlStateNormal];
}

- (void)configureLockscreenView
{
    self.lockscreenView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0f, CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetHeight([[UIScreen mainScreen] bounds]))];
    self.lockscreenView.userInteractionEnabled = NO;
    self.lockscreenView.hidden = YES;
    
    CGFloat xOffset = 0.5f * (CGRectGetWidth([[UIScreen mainScreen] bounds]) - 220.f);
    self.lockscreenTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 50.f, 220.f, 64.f)];
    self.lockscreenTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.lockscreenTimeLabel.font = [UIFont systemFontOfSize:87.0f weight:UIFontWeightUltraLight];
    [self.lockscreenView addSubview:self.lockscreenTimeLabel];
    
    self.lockscreenDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 128.f, 220.f, 22.f)];
    self.lockscreenDateLabel.textAlignment = NSTextAlignmentCenter;
    self.lockscreenDateLabel.font = [UIFont systemFontOfSize:18.0f weight:UIFontWeightRegular];
    [self.lockscreenView addSubview:self.lockscreenDateLabel];
    
    [self.view addSubview:self.lockscreenView];
}

- (void)tapHandler:(UITapGestureRecognizer *)recognizer
{
    if (self.imageCount == self.currentPageIndex) return;
    
    if ([self.lockscreenTimeLabel.textColor isEqual:[UIColor whiteColor]]) {
        self.lockscreenTimeLabel.textColor = [UIColor blackColor];
        self.lockscreenDateLabel.textColor = self.lockscreenTimeLabel.textColor;
        return;
    }
        
    [self.lockscreenView setHidden:!self.lockscreenView.hidden];
    
    if (!self.lockscreenView.hidden) {
        [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASPreviewLockScreen];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"H:mm"];
        self.lockscreenTimeLabel.text = [formatter stringFromDate:[NSDate date]];
        self.lockscreenTimeLabel.textColor = [UIColor whiteColor];
        
        NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEEMMMMd" options:0 locale:[NSLocale currentLocale]];
        [formatter setDateFormat:dateFormat];

        self.lockscreenDateLabel.text = [formatter stringFromDate:[NSDate date]];
        self.lockscreenDateLabel.textColor = self.lockscreenTimeLabel.textColor;
    }
}

- (void)swipeDownwardHandler:(UISwipeGestureRecognizer *)recognizer
{
    if (self.imageCount == self.currentPageIndex) return;
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        [self saveImage];
    }
}

#pragma mark - Menu Handlers

- (void)saveImage
{
    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASMenuItemSaveImage];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[ARWRUnsplashManager sharedManager] saveToCameraRollImageAtIndex:self.currentPageIndex];
}

- (void)openLink:(NSURL *)link
{
    [[UIApplication sharedApplication] openURL:link options:@{} completionHandler:^(BOOL success) {
        
    }];
}

#pragma mark - SCPageViewController DataSource

- (NSUInteger)numberOfPagesInPageViewController:(SCPageViewController *)pageViewController
{
    return (self.imageCount > 0) ? self.imageCount + 1 : self.imageCount;
}

- (UIViewController *)pageViewController:(SCPageViewController *)pageViewController viewControllerForPageAtIndex:(NSUInteger)pageIndex
{
    ARWRThumbViewController *viewController = self.viewControllers[pageIndex];
    
    if ([viewController isEqual:[NSNull null]]) {
        viewController = [[ARWRThumbViewController alloc] init];
        [viewController.view setFrame:self.view.bounds];
        [viewController.thumbView setContentMode:UIViewContentModeScaleAspectFill];
        [viewController.thumbView setClipsToBounds:YES];
        [viewController.thumbView
         setImageWithURLRequest:[[ARWRUnsplashManager sharedManager] urlRequestForThumbAtIndex:pageIndex]
         placeholderImage:nil
         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            if (self.authorView.hidden)
                self.authorView.hidden = NO;
            [viewController.thumbView setImage:image];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            LogRed(@"Failed with error %@.", error);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }];
        [viewController.thumbView setBackgroundColor:[UIColor arwrWhiteColor]];
        [self.viewControllers replaceObjectAtIndex:pageIndex withObject:viewController];
    }
    
    return viewController;
}

- (void)pageViewController:(SCPageViewController *)pageViewController didNavigateToPageAtIndex:(NSUInteger)pageIndex
{
    if (self.imageCount == 0) return;
    if ((self.currentPageIndex == self.imageCount - 1) && (pageIndex == self.imageCount)) {
        self.authorView.hidden = YES;
        [self.lockscreenView setHidden:YES];
        self.lockscreenTimeLabel.textColor = [UIColor blackColor];
        self.lockscreenDateLabel.textColor = self.lockscreenTimeLabel.textColor;
        self.currentPageIndex = pageIndex;
        return;
    }
    if (pageIndex == self.imageCount - 1) {
        self.authorView.hidden = NO;
    }
    self.currentPageIndex = pageIndex;
    if (pageIndex != self.imageCount) {
        [self updateAuthor:[[ARWRUnsplashManager sharedManager] authorOfImageAtIndex:pageIndex]];
    }
}

- (void)pageViewController:(SCPageViewController *)pageViewController didShowViewController:(UIViewController *)controller atIndex:(NSUInteger)pageIndex
{
    if (self.imageCount == 0) return;
    self.thumbsController.view.backgroundColor = [UIColor arwrWhiteColor];
}

#pragma mark - ARWRUnsplashManagerDelegate

- (void)managerDidLoadImages:(NSUInteger)count
{
    self.imageCount = count;
    for (int i = 0; i < self.imageCount; i++) {
        [self.viewControllers addObject:[NSNull null]];
    }
    
    ARWRAboutViewController *avc = [[ARWRAboutViewController alloc] initWithNibName:NSStringFromClass([ARWRAboutViewController class]) bundle:nil];
    [self.viewControllers addObject:avc];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownwardHandler:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeRecognizer];
    
    [self updateAuthor:[[ARWRUnsplashManager sharedManager] authorOfImageAtIndex:0]];
    [self.authorButton setEnabled:YES];
    [self.thumbsController reloadData];
    [self checkUpdate];
}

- (void)managerLoadImageAtIndex:(NSUInteger)imageIndex progress:(double)loadingProgress
{
    ARWRThumbViewController *viewController = self.viewControllers[imageIndex];
    [viewController.progressBar setProgress:loadingProgress animated:YES];
}

- (void)managerDidSaveImageAtIndex:(NSUInteger)imageIndex
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    ARWRThumbViewController *viewController = self.viewControllers[imageIndex];
    [viewController.progressBar performAction:M13ProgressViewActionSuccess animated:YES];
    [viewController performSelector:@selector(resetProgressBar) withObject:nil afterDelay:.3];
    
    [RKDropdownAlert title:NSLocalizedString(@"Yo!", nil)
                   message:NSLocalizedString(@"Image saved to Camera Roll", nil)
           backgroundColor:[UIColor arwrGreenColor]
                 textColor:[UIColor arwrWhiteColor]
                      time:ARWRNotificationDuration];
    
    if ([SKStoreReviewController class]) {
        if (arc4random_uniform(3) == 0) {
            [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASShowRate];
            [SKStoreReviewController requestReview];
        }
    }
}

- (void)managerDidSaveImageFailedAtIndex:(NSUInteger)imageIndex
{
    ARWRThumbViewController *viewController = self.viewControllers[imageIndex];
    [viewController.progressBar performAction:M13ProgressViewActionFailure animated:YES];
    [viewController performSelector:@selector(resetProgressBar) withObject:nil afterDelay:.3];
    
    [RKDropdownAlert title:NSLocalizedString(@"Ooops!", nil)
                   message:NSLocalizedString(@"Something went wrong. Please try again.", nil)
           backgroundColor:[UIColor arwrRedColor]
                 textColor:[UIColor arwrWhiteColor]
                      time:ARWRNotificationDuration];
}

- (void)managerNetworkReachable
{
}

- (void)managerNetworkUnreachable
{
    self.authorView.hidden = YES;

    self.backgroundImageView.image = [[UIImage imageNamed:@"icn_sad"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.backgroundImageView.layer addAnimation:transition forKey:nil];
    
    self.imageCount = 0;
    [self.thumbsController reloadData];
    
    [RKDropdownAlert title:NSLocalizedString(@"Network error", nil)
                   message:NSLocalizedString(@"Couldn't connect to the server. Check your network connection.", nil)
           backgroundColor:[UIColor arwrRedColor]
                 textColor:[UIColor arwrWhiteColor] time:60];
}

@end
