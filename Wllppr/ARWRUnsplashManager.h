//
//  ARWRUnsplashManager.h
//  Wllppr
//
//  Created by IT-EFFECTs on 20.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ARWRUnsplashManagerDelegate;

@interface ARWRUnsplashManager : NSObject

@property (nonatomic, weak, nullable) id <ARWRUnsplashManagerDelegate> delegate;

+ (ARWRUnsplashManager * _Nonnull)sharedManager;

- (NSURLRequest * _Nonnull)urlRequestForThumbAtIndex:(NSUInteger)index;
- (UIColor * _Nonnull)colorOfImageAtIndex:(NSUInteger)index;
- (NSString * _Nonnull)authorOfImageAtIndex:(NSUInteger)index;
- (NSURL * _Nonnull)linktToPageOfAuthorOfImageAtIndex:(NSUInteger)index;
- (void)saveToCameraRollImageAtIndex:(NSUInteger)index;
- (void)testNetwork;

@end

@protocol ARWRUnsplashManagerDelegate <NSObject>

- (void)managerDidLoadImages:(NSUInteger)count;
- (void)managerDidSaveImageAtIndex:(NSUInteger)imageIndex;
- (void)managerDidSaveImageFailedAtIndex:(NSUInteger)imageIndex;
- (void)managerNetworkReachable;
- (void)managerNetworkUnreachable;
- (void)managerLoadImageAtIndex:(NSUInteger)index progress:(double)loadingProgress;

@end
