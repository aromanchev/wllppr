//
//  ARWRUnsplashManager.m
//  Wllppr
//
//  Created by IT-EFFECTs on 20.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import "ARWRUnsplashManager.h"

#import <AFNetworking.h>
#import <Photos/Photos.h>

static NSString *const ARWRLastSessionPageCount = @"ARWRLastSessionPageCount";
static NSString *const ARWRLastPageNumber = @"ARWRLastPageNumber";
static NSString *const ARWRTodayDate = @"ARWRTodayDate";

static NSString * const WRPixabayAPIKey = @"8965509-5d260518db45933451b45b67a";
static NSString * const WRPixabayURL = @"https://pixabay.com/api/";
static NSString * const WRPixabayUserURL = @"https://pixabay.com/users/";

//static NSString * const WRProxyURL = @"https://http-cors-proxy.p.rapidapi.com/";
static NSString * const WRProxyURL = @"https://cors-proxy4.p.rapidapi.com/?";


@interface ARWRUnsplashManager ()

@property (nonatomic) NSMutableIndexSet *downloadedIndex;
@property (nonatomic) NSUInteger rateLimit;
@property (nonatomic) NSUInteger rateLimitRemaining;
@property (nonatomic) NSUInteger totalPageCount;
@property (nonatomic, strong) NSArray *page;

@end

@implementation ARWRUnsplashManager

+ (ARWRUnsplashManager *)sharedManager
{
    static ARWRUnsplashManager *sharedManager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self configureReachabilitySettings];
        self.page = [NSArray array];
        self.downloadedIndex = [NSMutableIndexSet indexSet];
    }
    return self;
}

#pragma mark - HTTP Requests

- (void)loadImages
{
    NSNumber *todayPage;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *today = [[NSUserDefaults standardUserDefaults] valueForKey:ARWRTodayDate];
    if ([today isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
        todayPage = [[NSUserDefaults standardUserDefaults] valueForKey:ARWRLastPageNumber];
    } else {
        NSUInteger lastPage = [[NSUserDefaults standardUserDefaults] integerForKey:ARWRLastPageNumber];
        todayPage = @(lastPage + 1);
        [[NSUserDefaults standardUserDefaults] setValue:todayPage forKey:ARWRLastPageNumber];
        [[NSUserDefaults standardUserDefaults] setValue:[formatter stringFromDate:[NSDate date]] forKey:ARWRTodayDate];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self loadImagesFromPage:todayPage.integerValue];
}

- (NSString *)stringFromURL:(NSString *)url withParameters:(NSDictionary *)parameters
{
    __block NSString *string = @"?";
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString * key, id  _Nonnull value, BOOL * _Nonnull stop) {
        string = [string stringByAppendingString:[NSString stringWithFormat:@"%@=%@&", key, value]];
    }];
    string = [string substringToIndex:string.length-1];
    string = [url stringByAppendingString:string];
    
    return string;
}

- (void)loadImagesFromPage:(NSUInteger)page
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) return;

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.operationQueue waitUntilAllOperationsAreFinished];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    NSDictionary *headers = @{};
    NSString *urlString = WRPixabayURL;
    NSDictionary *parameters = @{@"key": WRPixabayAPIKey,
                                 @"q": @"",
                                 @"safesearch": @"true",
                                 @"page": @(page),
                                 @"per_page": @(30)};
    if (needProxy) {
        headers = @{ @"origin": @"https://rapidapi.com/",
                     @"X-RapidAPI-Key": @"b95bfd9542msh12a1477b555982bp1c6b19jsn17399cfbbb4e",
                     @"X-RapidAPI-Host": @"cors-proxy4.p.rapidapi.com" };
        urlString = [WRProxyURL stringByAppendingString:urlString];
        NSString *proxyURL = [self stringFromURL:WRPixabayURL withParameters:parameters];
        parameters = @{@"url": proxyURL};
    }

    [manager GET:urlString parameters:parameters headers:headers progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASLoadingImagePageSuccess];
        self.page = responseObject[@"hits"];
        [self.delegate managerDidLoadImages:self.page.count];
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASLoadingInagePageFailed];
        if ([[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == 400) {
            [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:ARWRLastPageNumber];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self loadImagesFromPage:1];
        }
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
    }];
}

- (void)saveToCameraRollImageAtIndex:(NSUInteger)index
{
    if ([self.downloadedIndex containsIndex:index]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    [self.downloadedIndex addIndex:index];
    
    NSString *imageURL = self.page[index][@"imageURL"];
    NSDictionary *headers = @{};
    if (needProxy) {
        headers = @{ @"origin": @"https://rapidapi.com",
                     @"X-RapidAPI-Key": @"b95bfd9542msh12a1477b555982bp1c6b19jsn17399cfbbb4e",
                     @"X-RapidAPI-Host": @"cors-proxy4.p.rapidapi.com" };
        imageURL = [WRProxyURL stringByAppendingString:imageURL];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFImageResponseSerializer serializer];
    [manager GET:imageURL parameters:nil headers:headers progress:^(NSProgress * _Nonnull downloadProgress) {
        [self.delegate managerLoadImageAtIndex:index progress:downloadProgress.fractionCompleted];
    } success:^(NSURLSessionTask *task, id responseObject) {
        [self.downloadedIndex removeIndex:index];
        [self saveImage:responseObject atIndex:index];
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.downloadedIndex removeIndex:index];
        [self.delegate managerDidSaveImageFailedAtIndex:index];
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
    }];
}

#pragma mark - URL Requests

- (NSURLRequest *)urlRequestForThumbAtIndex:(NSUInteger)index
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *urlString = self.page[index][@"largeImageURL"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    if (needProxy) {
        [request setURL:[NSURL URLWithString:[WRProxyURL stringByAppendingString:urlString]]];
        [request setValue:@"https://rapidapi.com" forHTTPHeaderField:@"origin"];
        [request setValue:@"b95bfd9542msh12a1477b555982bp1c6b19jsn17399cfbbb4e" forHTTPHeaderField:@"X-RapidAPI-Key"];
        [request setValue:@"cors-proxy4.p.rapidapi.com" forHTTPHeaderField:@"X-RapidAPI-Host"];
    }
    return request;
}

#pragma mark - Save Image

- (void)saveImage:(UIImage *)image atIndex:(NSUInteger)index
{
    void (^authorizedBlock)(void) = ^{
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        } completionHandler:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASImageSavingSuccess];
                    [self.delegate managerDidSaveImageAtIndex:index];
                } else {
                    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASImageSavingFailed];
                    [self.delegate managerDidSaveImageFailedAtIndex:index];
                }
            });
        }];
    };
    void (^failedBlock)(void) = ^{
        [self.delegate managerDidSaveImageFailedAtIndex:index];
    };
    
    PHAuthorizationStatus authorizationStatus = [PHPhotoLibrary authorizationStatus];
    switch (authorizationStatus) {
        case PHAuthorizationStatusNotDetermined: {
            // permission dialog not yet presented, request authorization
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (status == PHAuthorizationStatusAuthorized) {
                        authorizedBlock();
                    } else {
                        failedBlock();
                    };
                });
            }];
            break;
        }
        case PHAuthorizationStatusAuthorized:
        case PHAuthorizationStatusLimited:
            // go ahead
            authorizedBlock();
            break;
        case PHAuthorizationStatusDenied:
        case PHAuthorizationStatusRestricted:
            // the user explicitly denied camera usage or is not allowed to access the camera devices
            failedBlock();
            break;
    }
}

#pragma mark - Color

- (UIColor *)colorOfImageAtIndex:(NSUInteger)index
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:self.page[index][@"color"]];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue >> 16) & 0xFF)/255.0 green:((rgbValue >> 8) & 0xFF)/255.0 blue:((rgbValue >> 0) & 0xFF)/255.0 alpha:1.0];
}

#pragma mark - Author

- (NSString *)authorOfImageAtIndex:(NSUInteger)index
{
    return self.page[index][@"user"];
}

- (NSURL *)linktToPageOfAuthorOfImageAtIndex:(NSUInteger)index
{
    return [NSURL URLWithString:[WRPixabayUserURL stringByAppendingString:[self authorOfImageAtIndex:index]]];
}

#pragma mark - Reachability

- (void)configureReachabilitySettings
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [[ARWRAnalyticsLogger sharedLogger] logEvent:AFStringFromNetworkReachabilityStatus(status)];
        if (status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown) {
            [self.delegate managerNetworkUnreachable];
        } else {
            [self loadImages];
            [self.delegate managerNetworkReachable];
        }
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

#pragma mark -

- (void)testNetwork
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.operationQueue waitUntilAllOperationsAreFinished];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager GET:[WRPixabayURL stringByAppendingString:@"docs/"] parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
        needProxy = false;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [manager invalidateSessionCancelingTasks:YES resetSession:NO];
        needProxy = true;
    }];
}

@end
