//
//  ARWRAnalyticsLogger.m
//  Wllppr
//
//  Created by IT-EFFECTs on 27.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import "ARWRAnalyticsLogger.h"

static NSString *const ARWRYandexApiKey = @"a2391e79-94c9-4617-b3d0-ceaed6fcc173";

@implementation ARWRAnalyticsLogger

+ (ARWRAnalyticsLogger *)sharedLogger
{
    static ARWRAnalyticsLogger *sharedLogger = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedLogger = [[self alloc] init];
    });
    return sharedLogger;
}

- (instancetype)init
{
    if (self = [super init]) {
        [YMMYandexMetrica activateWithConfiguration:[[YMMYandexMetricaConfiguration alloc] initWithApiKey:ARWRYandexApiKey]];
//        [ALSdk initializeSdk];
    }
    return self;
}

- (void)logEvent:(NSString *)event
{
//    LogGreen(@"%@", event);
    [YMMYandexMetrica reportEvent:event onFailure:^(NSError *error) {
        NSLog(@"DID FAIL REPORT EVENT: %@", event);
        NSLog(@"REPORT ERROR: %@", [error localizedDescription]);
    }];
}

@end
