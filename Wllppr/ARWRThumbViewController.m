//
//  ARWRThumbViewController.m
//  Wllppr
//
//  Created by IT-EFFECTs on 21.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import "ARWRThumbViewController.h"

@interface ARWRThumbViewController ()

@end

@implementation ARWRThumbViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int h = (IS_IPHONEX) ? 44 : 20;
    _progressBar = [[M13ProgressViewBorderedBar alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth([[UIScreen mainScreen] bounds]), h)];
    _progressBar.progressDirection = M13ProgressViewBorderedBarProgressDirectionLeftToRight;
    _progressBar.cornerType        = M13ProgressViewBorderedBarCornerTypeSquare;
    _progressBar.borderWidth       = 0.f;
    _progressBar.successColor      = [UIColor arwrGreenColor];
    _progressBar.failureColor      = [UIColor arwrRedColor];
    _progressBar.primaryColor      = [UIColor arwrGreenColor];
    [self.view addSubview:_progressBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)resetProgressBar
{
    [_progressBar performAction:M13ProgressViewActionNone animated:YES];
    [_progressBar setProgress:0.f animated:NO];
}

@end
