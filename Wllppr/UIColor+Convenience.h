//
//  UIColor+Convenience.h
//  PhotoFlipper
//
//  Created by Aleksandr Romanchev on 03.01.16.
//  Copyright © 2016 Aleksandr Romanchev. All rights reserved.
//

@import UIKit;

@interface UIColor (Convenience)

+ (UIColor *)arwrDarkBlackColor;
+ (UIColor *)arwrLightBlackColor;

+ (UIColor *)arwrGreenColor;
+ (UIColor *)arwrRedColor;
+ (UIColor *)arwrYellowColorTranslucent;
+ (UIColor *)arwrYellowColor;
+ (UIColor *)arwrWhiteColor;

@end
