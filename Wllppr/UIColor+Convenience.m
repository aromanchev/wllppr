//
//  UIColor+Convenience.m
//  PhotoFlipper
//
//  Created by Aleksandr Romanchev on 03.01.16.
//  Copyright © 2016 Aleksandr Romanchev. All rights reserved.
//

#import "UIColor+Convenience.h"

@implementation UIColor (Convenience)

+ (UIColor *)arwrDarkBlackColor
{
    return [UIColor colorWithRed:0.016 green:0.016 blue:0.004 alpha:1];
}

+ (UIColor *)arwrLightBlackColor
{
    return [UIColor colorWithRed:0.231 green:0.227 blue:0.208 alpha:1];
}

+ (UIColor *)arwrGreenColor
{
    return [UIColor colorWithRed:119.0/255 green:195.0/255 blue:89.0/255 alpha:1.0];
}

+ (UIColor *)arwrRedColor
{
    return [UIColor colorWithRed:237.0/255.0 green:73.0/255.0 blue:86.0/255.0 alpha:1.0];
}

+ (UIColor *)arwrYellowColor
{
    return [UIColor colorWithRed:1.0 green:0.800 blue:0.0 alpha:1];
}

+ (UIColor *)arwrYellowColorTranslucent
{
    return [UIColor colorWithRed:1.0 green:0.800 blue:0.0 alpha:0.5];
}

+ (UIColor *)arwrWhiteColor
{
    return [UIColor colorWithRed:0.973 green:0.973 blue:0.973 alpha:1];
}

@end
