//
//  ViewController.h
//  Wllppr
//
//  Created by IT-EFFECTs on 20.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARWRUnsplashManager.h"

@interface ARWRRootViewController : UIViewController <ARWRUnsplashManagerDelegate>


@end

