//
//  ARWRAnalyticsLogger.h
//  Wllppr
//
//  Created by IT-EFFECTs on 27.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ARWRAnalyticsStrings.h"

@interface ARWRAnalyticsLogger : NSObject

+ (ARWRAnalyticsLogger * _Nonnull)sharedLogger;
- (void)logEvent:(NSString * _Nonnull)event;

@end
