//
//  main.m
//  Wllppr
//
//  Created by IT-EFFECTs on 20.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARWRAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARWRAppDelegate class]));
    }
}
