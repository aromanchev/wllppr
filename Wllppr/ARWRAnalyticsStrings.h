//
//  ARWRAnalyticsStrings.h
//  Wllppr
//
//  Created by IT-EFFECTs on 27.10.15.
//  Copyright © 2015 Aleksandr Romanchev. All rights reserved.
//

#ifndef ARWRAnalyticsStrings_h
#define ARWRAnalyticsStrings_h

static NSString *const ARWRASLoadingImagePageSuccess = @"Loading Image Page Success";
static NSString *const ARWRASLoadingInagePageFailed  = @"Loading Image Page Failed";

static NSString *const ARWRASRequestLimitExceeded = @"Warning: Request Limit Exceeded";

static NSString *const ARWRASImageSavingSuccess = @"Image Saving Success";
static NSString *const ARWRASImageSavingFailed  = @"Image Saving Failed";
static NSString *const ARWRASShowRate = @"Rate Prompt";
static NSString *const ARWRASPreviewLockScreen  = @"Preview Lock Screen";

static NSString *const ARWRASOpenAuthorPageOnUnsplash = @"Open Author Page on Unsplash";

static NSString *const ARWRASMenuItemTellFriends   = @"Menu Item Tell Friends Pressed";
static NSString *const ARWRASMenuItemRateApp       = @"Menu Item Rate App Pressed";
static NSString *const ARWRASMenuItemMailDeveloper = @"Menu Item Mail to Developer Pressed";
static NSString *const ARWRASMenuItemOtherApps     = @"Menu Item Other Apps Pressed";
static NSString *const ARWRASMenuItemSaveImage     = @"Menu Item Save Image Pressed";

#endif /* ARWRAnalyticsStrings_h */
