//
//  ARWRAboutViewController.m
//  Wllppr
//
//  Created by Aleksandr Romanchev on 16.02.16.
//  Copyright © 2016 Aleksandr Romanchev. All rights reserved.
//

#import "ARWRAboutViewController.h"

#import "UIButton+BBCBackgroundColor.h"
#import <MessageUI/MessageUI.h>

static NSString *const ARWRApplicationLink = @"https://itunes.apple.com/app/id1053066153";
static NSString *const ARWRApplicationRateLink = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1053066153&pageNumber=0&sortOrdering=2&mt=8";
static NSString *const ARWRDeveloperLink = @"itms-apps://itunes.apple.com/artist/id986445989";
static NSString *const ARWRDeveloperSupportMail = @"support@kcandr.ru";

@interface ARWRAboutViewController () <MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;

@property (nonatomic, weak) IBOutlet UIButton *otherButton;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UIButton *contactButton;
@property (nonatomic, weak) IBOutlet UIButton *rateButton;

@end

@implementation ARWRAboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor arwrWhiteColor]];
        
    [_descriptionLabel setNumberOfLines:3];
    [_descriptionLabel setText:NSLocalizedString(@"About", nil)];
    [_descriptionLabel setTextColor:[UIColor arwrLightBlackColor]];
    [_descriptionLabel setTextAlignment:NSTextAlignmentCenter];
    
    [_otherButton.layer setBorderWidth:1.f];
    [_otherButton.layer setCornerRadius:4.f];
    [_otherButton.layer setBorderColor:[UIColor arwrLightBlackColor].CGColor];
    [_otherButton setTitleColor:[UIColor arwrLightBlackColor] forState:UIControlStateNormal];
    [_otherButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateHighlighted];
    [_otherButton setTitle:NSLocalizedString(@"Our apps", nil) forState:UIControlStateNormal];
    [_otherButton addTarget:self action:@selector(otherHandler) forControlEvents:UIControlEventTouchUpInside];
    [_otherButton bbc_backgroundColorNormal:[UIColor clearColor] backgroundColorSelected:[UIColor arwrLightBlackColor]];
    
    [_contactButton.layer setBorderWidth:1.f];
    [_contactButton.layer setCornerRadius:4.f];
    [_contactButton.layer setBorderColor:[UIColor arwrLightBlackColor].CGColor];
    [_contactButton setTitleColor:[UIColor arwrLightBlackColor] forState:UIControlStateNormal];
    [_contactButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateHighlighted];
    [_contactButton setTitle:NSLocalizedString(@"Contact with developer", nil) forState:UIControlStateNormal];
    [_contactButton addTarget:self action:@selector(mailToSupport) forControlEvents:UIControlEventTouchUpInside];
    [_contactButton bbc_backgroundColorNormal:[UIColor clearColor] backgroundColorSelected:[UIColor arwrLightBlackColor]];
    
    [_shareButton.layer setBorderWidth:1.f];
    [_shareButton.layer setCornerRadius:4.f];
    [_shareButton.layer setBorderColor:[UIColor arwrLightBlackColor].CGColor];
    [_shareButton setTitleColor:[UIColor arwrLightBlackColor] forState:UIControlStateNormal];
    [_shareButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateHighlighted];
    [_shareButton setTitle:NSLocalizedString(@"Share this app", nil) forState:UIControlStateNormal];
    [_shareButton addTarget:self action:@selector(tellFriends) forControlEvents:UIControlEventTouchUpInside];
    [_shareButton bbc_backgroundColorNormal:[UIColor clearColor] backgroundColorSelected:[UIColor arwrLightBlackColor]];
    
    [_rateButton.layer setCornerRadius:4.f];
    [_rateButton setBackgroundColor:[UIColor arwrYellowColor]];
    [_rateButton setTitleColor:[UIColor arwrLightBlackColor] forState:UIControlStateNormal];
    [_rateButton setTitleColor:[UIColor arwrWhiteColor] forState:UIControlStateHighlighted];
    [_rateButton setTitle:NSLocalizedString(@"Rate us on the App Store", nil) forState:UIControlStateNormal];
    [_rateButton addTarget:self action:@selector(rateHandler) forControlEvents:UIControlEventTouchUpInside];
    [_rateButton bbc_backgroundColorNormal:[UIColor arwrYellowColor] backgroundColorSelected:[UIColor arwrYellowColorTranslucent]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)buttonHighlight:(UIButton *)sender
{
    [sender setBackgroundColor:[UIColor arwrWhiteColor]];
}

- (void)buttonNormal:(UIButton *)sender
{
    [sender setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)openLink:(NSURL *)link
{
    [[UIApplication sharedApplication] openURL:link options:@{} completionHandler:^(BOOL success) {
    }];
}

- (void)rateHandler
{
    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASMenuItemRateApp];
    [self openLink:[NSURL URLWithString:ARWRApplicationRateLink]];
}

- (void)otherHandler
{
    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASMenuItemOtherApps];
    [self openLink:[NSURL URLWithString:ARWRDeveloperLink]];
}

- (void)tellFriends
{
    [[ARWRAnalyticsLogger sharedLogger] logEvent:ARWRASMenuItemTellFriends];

    NSString *text = @"High-resolution wallpapers in 'wllppr' app in AppStore";
    NSURL *url = [NSURL URLWithString:ARWRApplicationLink];
    UIImage *image = [UIImage imageNamed:@"iTunesArtwork"];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[text, url, image]
                                                                             applicationActivities:nil];
    controller.popoverPresentationController.sourceView = self.view;
    controller.popoverPresentationController.sourceRect = _contactButton.frame;
    [controller setValue:NSLocalizedString(@"Recommend 'wllppr'", @"Recommend 'wllppr'") forKey:@"subject"];
    
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypeAirDrop];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)mailToSupport
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        emailDialog.mailComposeDelegate = self;
        [emailDialog setToRecipients:@[ARWRDeveloperSupportMail]];
        [emailDialog setSubject:NSLocalizedString(@"Application 'wllppr'", @"Application 'wllppr'")];
        [emailDialog setMessageBody:NSLocalizedString(@"Tell us what you like in an application or don't and why.", @"Tell us what you like in an application or don't and why.") isHTML:NO];
        [self presentViewController:emailDialog animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    if(error) {
        NSLog(@"ERROR - mailComposeController: %@", [error localizedDescription]);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

@end
